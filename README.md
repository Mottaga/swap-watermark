# Swap Watermark

Watermarking technique based on the DCT

## Name
Swap Algorithm

## Description
Watermarking algorithm based on the DCT and the concept of swap the DCT coefficients

## Authors
1. Gabriele Motta : gabri1540@gmail.com
2. Michele Panariello : michpanariello@gmail.com
3. Nicola Arpino : nicolatosh4@gmail.com
4. Alessandra Morellini : alessandra.morellini@gmail.com

## License
The MIT License (MIT)

Copyright (c) 2021 Gabriele Motta, Michele Panariello, Nicola Arpino, Alessandra Morellini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


## Project status
Stable
